= POC Testing with GitLab API Fuzzing

This project has several branches showing different configurations.
All configurations target a simple Flask REST API.

 * `openapi` -- API Fuzzing using an OpenAPI v2 specification
 * `har` -- API Fuzzing using an HTTP Archive (HAR)
